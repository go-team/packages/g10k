% G10K(1) __VERSION__ | User Commands
%
% January 2019

# NAME

**g10k** -  Puppet environment and module deployment

# SYNOPSIS

| **g10k** \[_OPTIONS_]

| **g10k** **\–version**

# DESCRIPTION

**g10k** is a tool for deploying Puppet environments and modules, much
like r10k.  Its an almost drop-in replacement for r10k with additional
features such as caching, version pre-checking and distinct SSH keys
for each source. In general it also a lot faster than the original
r10k.

# OPTIONS
-branch string
: which git branch of the Puppet environment to update, e.g. core_foobar

-cachedir string
: allows overriding of the g10k configuration file cachedir setting, the folder in which g10k will download git repositories and Forge modules

-check4update
: only check if the is newer version of the Puppet module available. Does implicitly set dryrun to true

-checksum
: get the MD5 check sum for each Puppetlabs Forge module and verify the integrity of the downloaded archive. Increases g10k run time!

-config string
: which configuration file to use

-debug
: log debug output, defaults to false

-dryrun
: do not modify anything, just print what would be changed

-force
: purge the Puppet environment directory and do a full sync

-gitobjectsyntaxnotsupported
: if your git version is too old to support reference syntax like master^{object} use this setting to revert to the older syntax

-info
: log info output, defaults to false

-maxextractworker int
: how many Goroutines are allowed to run in parallel for local Git and Forge module extracting processes (git clone, untar and gunzip) (default 20)

-maxworker int
: how many Goroutines are allowed to run in parallel for Git and Forge module resolving (default 50)

-module string
: which module of the Puppet environment to update, e.g. stdlib

-moduledir string
: allows overriding of Puppetfile specific moduledir setting, the folder in which Puppet modules will be extracted

-outputname string
: overwrite the environment name if -branch is specified

-puppetfile
: install all modules from Puppetfile in CWD

-puppetfilelocation string
:  which Puppetfile to use in -puppetfile mode (default "./Puppetfile")

-quiet
: no output, defaults to false

-retrygitcommands
: if g10k should purge the local repository and retry a failed git command (clone or remote update) instead of failing

-tags
: to pull tags as well as branches

-usecachefallback
: if g10k should try to use its cache for sources and modules instead of failing

-usemove
: do not use hardlinks to populate your Puppet environments with Puppetlabs Forge modules. Instead uses simple move commands and purges the Forge cache directory after each run! (Useful for g10k runs inside Docker container)

-verbose
: log verbose output, defaults to false

-version
: show build time and version number

# NOTES

- Before using g10k with a large Puppet setup with many modules, be sure to increase the amount of open file handles (nfiles) and number of child processes (nproc), see limits.conf(5) for details.
- If you are using a private Git or Forge server think about adjusting the `-maxworker` parameter before DOSing your own infrastructure (default 50)
- To protect your local machine use `-maxextractworker` parameter with which you can limit the number of Goroutines that are allowed to run in parallel for local Git and Forge module extracting processes (git clone, untar and gunzip) (default 20)
- To use g10k behind a proxy, set the environment variables `http_proxy` or `https_proxy`. E.g. `http_proxy=http://proxy.domain.tld:8080 ./g10k -puppetfile`. See https://golang.org/pkg/net/http/#ProxyFromEnvironment for details.

# ADDITIONAL PUPPETFILE FEATURES

- link Git module branch to the current environment branch:
```
mod 'awesomemodule',
    :git => 'http://github.com/foo/bar.git',
    :link => 'true'
```
If you are in environment branch `dev` then g10k would try to check out this module with branch `dev`.
This helps to be able to use the same Puppetfile over multiple environment branches and makes merges easier.
See https://github.com/xorpaul/g10k/issues/6 for details.

Now also supports the r10k setting name `:branch => :control_branch` See https://github.com/xorpaul/g10k/issues/73

- only clone if branch/tag/commit exists
```
mod 'awesomemodule',
    :git => 'http://github.com/foo/bar.git',
    :ignore-unreachable => 'true'
```

In combination with the previous link feature you don't need to keep all environment branches also available for your modules.
See https://github.com/xorpaul/g10k/issues/9 for details.

- use different Forge base URL for your modules in your Puppetfile
```
forge.baseUrl http://foobar.domain.tld/
```
- skip version checks for latest Forge modules for a certain time to speed up the sync
```
forge.cacheTtl 4h
```
You need to specify the TTL value in the form of golang Duration (https://golang.org/pkg/time/#ParseDuration)

- try multiple Git branches for a Puppet module until one can be used
```
mod 'stdlib',
    :git => 'https://github.com/puppetlabs/puppetlabs-stdlib.git',
    :fallback => '4.889.x|foobar|master'
```

In this example g10k tries to use the branches:

`4.889.x` -> `foobar` -> `master`

Because there are no branches `4.889.x` or `foobar`.

All without failing or error messages.

Tip: You can see which branch was used, when using the `-verbose` parameter:

```
./g10k -puppetfile -verbose
2016/11/08 14:16:40 Executing git --git-dir ./tmp/https-__github.com_puppetlabs_puppetlabs-stdlib.git remote update --prune took 1.05001s
2016/11/08 14:16:40 Executing git --git-dir ./tmp/https-__github.com_puppetlabs_puppetlabs-stdlib.git log -n1 --pretty=format:%H master took 0.00299s
Synced ./Puppetfile with 4 git repositories and 0 Forge modules in 1.1s with git (1.1s sync, I/O 0.0s) and Forge (0.0s query+download, I/O 0.0s)
```

Now also supports the r10k setting name `:default_branch => 'master'` See https://github.com/xorpaul/g10k/issues/73

- additional Forge attribute `:sha256sum`:

For (some) increased security you can add a SHA256 sum for each Forge module, which g10k will verify after downloading the respective .tar.gz file:

```
mod 'puppetlabs/ntp', '6.0.0', :sha256sum => 'a988a172a3edde6ac2a26d0e893faa88d37bc47465afc50d55225a036906c944'

```

This does provide a very crude way to detect manipulated Forge modules and MITM attacks until the Puppetlabs Forge does support some sort of signing of Forge module releases.

If the SHA256 sum does not match the expected hash sum, g10k will warn the user and retry a download until giving up:

```
Resolving Forge modules (0/1)   --- [--------------------------------------------------------------------]   0%
WARNING: calculated sha256sum a988a172a3edde6ac2a26d0e893faa88d37bc47465afc50d55225a036906c944 for ./tmp/puppetlabs-ntp-6.0.0.tar.gz does not match expected sha256sum a988a172a3edde6ac2a26d0e893faa88d37bc47465afc50d55225a036906c94
Resolving Forge modules (0/1)   --- [--------------------------------------------------------------------]   0%
WARNING: calculated sha256sum a988a172a3edde6ac2a26d0e893faa88d37bc47465afc50d55225a036906c944 for ./tmp/puppetlabs-ntp-6.0.0.tar.gz does not match expected sha256sum a988a172a3edde6ac2a26d0e893faa88d37bc47465afc50d55225a036906c94
2016/12/08 18:05:11 downloadForgeModule(): giving up for Puppet module puppetlabs-ntp version: 6.0.0

```

(The Forge module retry count in case the Puppetlabs Forge provided MD5 sum, file archive size or SHA256 sum doesn't match defaults to `1`, but will be user configurable later.)

- override g10k cache directory with environment variable

You can use the following environment variable to make g10k use a different cache directory:

```
g10k_cachedir=/var/tmp g10k ...
```

This will also override the `-cachedir` parameter.


# ADDITIONAL CONFIGURATION

- you can enforce version numbers of Forge modules in your Puppetfiles instead of `:latest` or `:present` by adding `force_forge_versions: true` to the g10k config in the specific resource

```
---
:cachedir: '/tmp/g10k'

sources:
  example:
    remote: 'https://github.com/xorpaul/g10k-environment.git'
    basedir: '/tmp/example/'
    force_forge_versions: true
```

If g10k then encounters `:latest` or `:present` for a Forge module it errors out with:

```
2016/11/15 18:45:38 Error: Found present setting for forge module in /tmp/example/example_benchmark/Puppetfile for module puppetlabs/concat line: mod 'puppetlabs/concat' and force_forge_versions is set to true! Please specify a version (e.g. '2.3.0')
```

- g10k can let you know if your source does not contain the branch you specified with the `-branch` parameter:

```
---
:cachedir: '/tmp/g10k'

sources:
  example:
    remote: 'https://github.com/xorpaul/g10k-environment.git'
    basedir: '/tmp/example/'
    warn_if_branch_is_missing: true
```

If you then call g10k with this configuration file and the following parameter `-branch nonExistingBranch`. You should get:

```
WARNING: Couldn't find specified branch 'nonExistingBranch' anywhere in source 'example' (https://github.com/xorpaul/g10k-environment.git)
```

This can be helpful if you use a dedicated Hiera repository/g10k source and you want to ensure that you always have a matching branch, see https://github.com/xorpaul/g10k/issues/45

- By default g10k fails if one of your Puppet environments could not be completely populated (e.g. if one of your Puppet Git module branches doesn't exist anymore). You can change this by setting `ignore_unreachable_modules` to true in your g10k configuration:

```
---
:cachedir: '/tmp/g10k'
ignore_unreachable_modules: true

sources:
  example:
    remote: 'https://github.com/xorpaul/g10k-failing-env.git'
    basedir: '/tmp/failing/'
```

If you then call g10k with this configuration file and `debug` verbosity level, you should get:

```
DEBUG: Failed to populate module /tmp/failing/master/modules//sensu/ but ignore-unreachable is set. Continuing...
```

See https://github.com/xorpaul/g10k/issues/57 for details.

- abort g10k run if source repository is unreachable

```
---
:cachedir: '/tmp/g10k'

sources:
  example:
    remote: 'git://github.com/xorpaul/g10k-environment-unavailable.git'
    basedir: '/tmp/example/'
    exit_if_unreachable: true
```

If you then call g10k with this configuration file. You should get:

```
WARN: git repository git://github.com/xorpaul/g10k-environment-unavailable.git does not exist or is unreachable at this moment!
WARNING: Could not resolve git repository in source 'example' (git://github.com/xorpaul/g10k-environment-unavailable.git)
```
with an exit code 1

- g10k can use the cached version of Forge and git modules if their sources are currently not available:

```
---
:cachedir: '/tmp/g10k'
use_cache_fallback: true

sources:
  example:
    remote: 'git://github.com/xorpaul/g10k-environment-unavailable.git'
    basedir: '/tmp/example/'
```

If you then call g10k with this configuration file and your github.com repository is unavailable your g10k run tries to find a suitable cached version of your modules:

```
WARN: git repository https://github.com/puppetlabs/puppetlabs-firewall.git does not exist or is unreachable at this moment!
WARN: Trying to use cache for https://github.com/puppetlabs/puppetlabs-firewall.git git repository
```
if your g10k did manage to at least once cache this git repository.

If there is no usable cache available your g10k run still fails.

- You can let g10k retry to git clone or update the local repository if it failed before and was left in a corrupted state:

```
---
:cachedir: '/tmp/g10k'
retry_git_commands: true

sources:
  example:
    remote: 'https://github.com/xorpaul/g10k-environment.git'
    basedir: '/tmp/example/'
```

If you then call g10k with this configuration file and have a corrupted local Git repository, g10k deletes the local cache and retries the Git clone command once:

```
WARN: git command failed: git --git-dir /tmp/g10k/modules/https-__github.com_puppetlabs_puppetlabs-firewall.git remote update --prune deleting local cached repository and retrying...
```

See https://github.com/xorpaul/g10k/issues/76 for details.

- Autocorrecting Puppet environment names

Like in r10k (https://github.com/puppetlabs/r10k/blob/master/doc/dynamic-environments/git-environments.mkd#invalid_branches) for each source in your g10k configuration you can set the attribute `invalid_branches` with the following values:

  * `correct_and_warn`: Non-word characters will be replaced with underscores and a warning will be emitted.
  * `correct`: Non-word characters will silently be replaced with underscores.
  * `error`: Branches with non-word characters will be ignored and an error will be emitted.

The default value is to leave the environment unchanged, which differs from the r10k default!

Example:
```
---
:cachedir: '/tmp/g10k'

sources:
  example:
    remote: 'https://github.com/xorpaul/g10k-environment.git'
    basedir: '/tmp/example/'
    invalid_branches: 'correct'
```

If you then call g10k with this configuration file and have a branch named something like `single_autocorrect-%-fooo` it will be renamed to `single_autocorrect___fooo`

See https://github.com/xorpaul/g10k/issues/81 for details.

# COPYRIGHT

Copyright (c) 2019 Jack Henschel

# AUTHOR

This manual page is based on the g10k documentation. It was created by Jack Henschel <jackdev@mailbox.org> for the Debian GNU/Linux system, but may be used by others.

# SEE ALSO

**r10k(1)**
